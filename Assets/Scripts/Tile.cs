﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int value;
    public bool isTrap = false;
    public bool characterInit = false;
    public bool pursuerInit = false;
    public Tile nextTile;

    private MeshRenderer mRenderer;
    
    private void Awake()
    {
        mRenderer = GetComponent<MeshRenderer>();
        if (characterInit)
        {
            mRenderer.material = Resources.Load<Material>("Materials/BlueMat") as Material;
        }
        else if (pursuerInit)
        {
            mRenderer.material = Resources.Load<Material>("Materials/WhiteMat") as Material;
        }

        if (isTrap)
        {
            mRenderer.material = Resources.Load<Material>("Materials/RedMat") as Material;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void Setup(int value, Tile next)
    {
        this.value = value;
        transform.GetChild(0).GetComponent<TileTextValue>().Setup(value.ToString());

        nextTile = next;
    }
}
