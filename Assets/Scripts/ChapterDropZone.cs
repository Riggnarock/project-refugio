﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChapterDropZone : DropZone
{
    public GameObject cardPrefab;
    public override void OnDropAction(GameObject obj)
    {
        
        Transform tCard =obj.transform;
        tCard.SetParent(gameObject.transform);
        tCard.GetComponent<ChapterCardDisplay>().onHand = false;
        tCard.localScale = scaleVec;
        tCard.GetComponent<PlayingChapterCard>().Init();

        /*
        GameObject playChapterCard = Instantiate(cardPrefab, transform);
        PlayingChapterCard comp = playChapterCard.GetComponent<PlayingChapterCard>();
        comp.chapterCard = obj.GetComponent<ChapterCardDisplay>();
        comp.Init();
        playChapterCard.transform.localScale = scaleVec;
        obj.transform.SetParent(playChapterCard.transform);
        obj.transform.localPosition = Vector3.zero;
        obj.GetComponent<ChapterCardDisplay>().onHand = false;
        //obj.GetComponent<ChapterCardDisplay>().initialPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;
        */
        
    }
}
