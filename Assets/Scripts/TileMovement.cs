﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovement : MonoBehaviour
{
    public Tile currentTile;
    public Tile nextTile;
    public Tile targetTile;

    Vector3 velocity;
    float smoothTime = 0.25f;
    float smoothDistance = 0.01f;
    public bool canMove = false;
    //public bool isCharacter = false;
    //public bool isPursuer = false;

    // Start is called before the first frame update
    void Start()
    {
        //transform.position = waypoints[waypointIndex].position;
        //targetPosition = transform.position;
    }

    public void Setup(Tile tile)
    {
        currentTile = tile;
        nextTile = null;
        targetTile = currentTile;
        transform.position = currentTile.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Move();
        }
    }

    public void SetMoves(int moves)
    {
        print("Setting some more movesss " + moves);
        //targetWaypointIndex += moves;
        //if (targetWaypointIndex >= waypoints.Length) targetWaypointIndex = waypoints.Length - 1;

        Tile finalTile = targetTile;
        bool final = false;
        for (int i = 0; !final && i < moves; i++)
        {
            if (finalTile.nextTile != null) finalTile = finalTile.nextTile;
            else final = true;
        }
        targetTile = finalTile;
        SetNextTile(currentTile.nextTile);
    }

    private void SetNextTile(Tile tile)
    {
        if (tile == null)
        {
            canMove = false;
        }
        nextTile = tile;
        velocity = Vector3.zero;
    }

    private void Move()
    {
        if (Vector3.Distance(transform.position, nextTile.transform.position) < smoothDistance)
        {
            // tile reached. Check if we've reached the target tile
            currentTile = nextTile;
            if (currentTile == targetTile)
            {
                // we've reached the target tile
                canMove = false;
                //nextTile = null;
                // check if we landed in a trap
                if (currentTile.isTrap) GameControl.MovePursuer(1);
            }
            else if (currentTile.nextTile != null)
            {
                SetNextTile(currentTile.nextTile);
            }
            else canMove = false;
        }

        transform.position = Vector3.SmoothDamp(transform.position, nextTile.transform.position, ref velocity, smoothTime);
    }
}
