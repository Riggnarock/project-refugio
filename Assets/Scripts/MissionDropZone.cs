﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MissionDropZone : DropZone, IPointerEnterHandler, IPointerExitHandler
{
    public PlayingChapterCard chapter;
    CanvasGroup group;
    public bool pointed = false;

    private void Awake()
    {
        group = gameObject.GetComponent<CanvasGroup>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnDropAction(GameObject obj)
    {
        Debug.Log("Escape card on mission panel");
        /*Transform t = obj.transform;
        t.SetParent(gameObject.transform);
        t.GetComponent<EscapeCardDisplay>().onHand = false;
        t.localScale = scaleVec;*/

        //GameObject o = Instantiate(obj, gameObject.transform);
        //o.GetComponent<EscapeCardDisplay>().onHand = false;
        //o.GetComponent<EscapeCardDisplay>().card = obj

        if (chapter.CanAddCard((EscapeCard) obj.GetComponent<EscapeCardDisplay>().card))
        {
            obj.transform.localScale = chapter.transform.localScale * scaleAmount;
            chapter.AddCard(obj);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if (pointed) return;
        Debug.Log("pointer enter");
        pointed = true;
        GameObject obj = eventData.pointerDrag;
        if (obj != null)
        {
            try
            {
                if (obj.GetComponent<CardDisplay>().card.cardType == zoneType)
                {
                    //group.blocksRaycasts = true;
                } else
                {
                    //group.blocksRaycasts = false;
                }
            } catch (UnityException e)
            {
                Debug.LogError(e);
                return;
            }
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        if (pointed)
        {
            pointed = false;
            //group.blocksRaycasts = true;
        }
    }
}
