﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardChapterDisplay : CardDisplay
{
    public Text chapterNameBack;

    // Start is called before the first frame update
    protected override void Start()
    {
        chapterNameBack.text = ((ChapterCard)card).chapterName;
        base.Start();
    }
}
