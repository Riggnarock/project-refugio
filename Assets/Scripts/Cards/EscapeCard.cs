﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Escape Card", menuName = "Card/EscapeCard")]
public class EscapeCard : Card
{
    public string color;
    public int value;
    public Sprite artwork;
    public string chapter;

    public EscapeCard Init(int id, string chapter, string color, int value)
    {
        this.id = id;
        this.cardType = Card.CardType.ESCAPE;
        this.chapter = chapter;
        this.color = color;
        this.value = value;

        return this;
    }

    public override void Print()
    {
        Debug.Log("Escape card " + id + " color " + color + "steps" + value);
    }
}
