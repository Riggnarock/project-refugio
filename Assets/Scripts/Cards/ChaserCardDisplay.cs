﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChaserCardDisplay : CardDisplay
{
    public Text[] valuesNormal;
    public Text[] valuesPlus;
    public GameObject[] iconCards;

    // Start is called before the first frame update
    protected override void Start()
    {
        card = (ChaserCard) CardDataBase.cardList[cardId];
        if (((ChaserCard)card).type == "numerical")
        {
            foreach (GameObject iconCard in iconCards)
                iconCard.SetActive(false);
            foreach (Text valueNormal in valuesNormal)
                valueNormal.text = ((ChaserCard)card).tilesToMove.ToString();
        }
        else
        {
            foreach (GameObject iconCard in iconCards)
                iconCard.SetActive(true);
            if (((ChaserCard)card).tilesToMove > 0)
            {
                foreach (Text valuePlus in valuesPlus)
                    valuePlus.text = "+" + ((ChaserCard)card).tilesToMove.ToString();
            }
        }
        base.Start();
    }

    
}
