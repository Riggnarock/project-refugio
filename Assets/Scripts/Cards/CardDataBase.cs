﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CardDataBase : MonoBehaviour
{
    private static string DBPath = "Assets/Resources/Database/";
    public static List<Card> cardList = new List<Card>();
    //public List<ChapterCard> chapterCardList;
    //public List<EscapeCard> escapeCardList;
    //public List<ChaserCard> pursuerCardList;
    static int[] escapeCardsIndex;
    static int[] chapterCardsIndex;
    static int[] pursuerCardsIndex;
    static Dictionary<int, int[]> chaptersDict = new Dictionary<int, int[]>();

    // DEBUG
    public int[] publicEscapeCardsIndex;
    public int[] publicChapter0CardsIndex;
    public int[] publicChapter1CardsIndex;
    public int[] publicChapter2CardsIndex;
    public int[] publicPursuerCardsIndex;

    public int lastIndex = 0;

    private void Awake()
    {
        ReadEscapeCSV();
        publicEscapeCardsIndex = escapeCardsIndex;
        ReadChapterCSV();
        //publicChapterCardsIndex = chapterCardsIndex;
        publicChapter0CardsIndex = chaptersDict[0];
        publicChapter1CardsIndex = chaptersDict[1];
        publicChapter2CardsIndex = chaptersDict[2];
        ReadPursuerCSV();
        lastIndex--;
        publicPursuerCardsIndex = pursuerCardsIndex;

    }

    void ReadEscapeCSV()
    {
        StreamReader sReader = new StreamReader(DBPath + "Escape.csv");

        List<int> escapeIndexList = new List<int>();
        int count = 0;
        string dataLine = sReader.ReadLine();
        while (dataLine != null)
        {
            string[] dataValues = dataLine.Split(',');
            if (count > 0)
            {
                cardList.Add(ParseEscapeCard(dataValues));
                escapeIndexList.Add(lastIndex);
                lastIndex++;
            }
            count++;
            dataLine = sReader.ReadLine();
        }
        escapeCardsIndex = escapeIndexList.ToArray();
    }

    void ReadChapterCSV()
    {
        StreamReader sReader = new StreamReader(DBPath + "Chapter.csv");

        int chapter = -1;
        string chapterName = "";
        List<int> currentChapterIndex = new List<int>();
        int count = 0;
        string dataLine = sReader.ReadLine();
        while (dataLine != null)
        {
            string[] dataValues = dataLine.Split(',');
            if (count > 1)
            {
                // check chapter change
                if (chapterName != dataValues[1])
                {
                    if (chapter >= 0)
                    {
                        chaptersDict[chapter] = currentChapterIndex.ToArray();
                        currentChapterIndex = new List<int>();
                    }
                    chapter++;
                    chapterName = dataValues[1];
                }

                cardList.Add(ParseChapterCard(dataValues));
                //chapterIndexList.Add(lastIndex);
                currentChapterIndex.Add(lastIndex);
                lastIndex++;
            }
            count++;
            dataLine = sReader.ReadLine();
        }
        //chapterCardsIndex = chapterIndexList.ToArray();
    }

    void ReadPursuerCSV()
    {
        StreamReader sReader = new StreamReader(DBPath + "Pursuer.csv");

        List<int> pursuerIndexList = new List<int>();
        int count = 0;
        string dataLine = sReader.ReadLine();
        while (dataLine != null)
        {
            string[] dataValues = dataLine.Split(',');
            if (count > 0)
            {
                cardList.Add(ParsePursuerCard(dataValues));
                pursuerIndexList.Add(lastIndex);
                lastIndex++;
            }
            count++;
            dataLine = sReader.ReadLine();
        }

        pursuerCardsIndex = pursuerIndexList.ToArray();
    }

    EscapeCard ParseEscapeCard(string[] csvData)
    {
        return ScriptableObject.CreateInstance<EscapeCard>().Init(int.Parse(csvData[0]), csvData[3], csvData[1], int.Parse(csvData[2]));
    }

    ChapterCard ParseChapterCard(string[] csvData)
    {
        List<string> colors = new List<string>();
        for (int i = 3; i < csvData.Length; ++i)
        {
            if (csvData[i] != "")
            {
                colors.Add(csvData[i].Trim());
            }
        }
        return ScriptableObject.CreateInstance<ChapterCard>().Init(int.Parse(csvData[0]), csvData[1], int.Parse(csvData[2]), colors.ToArray());
    }

    ChaserCard ParsePursuerCard(string[] csvData)
    {
        return ScriptableObject.CreateInstance<ChaserCard>().Init(int.Parse(csvData[0]), int.Parse(csvData[2]), csvData[1]);
    }

    public static List<Card> GetEscapeCards()
    {
        List<Card> escapeCards = new List<Card>();
        foreach (int index in escapeCardsIndex)
        {
            escapeCards.Add(cardList[index]);
        }
        return escapeCards;
    }

    public static List<int> GetEscapeIndex()
    {
        return new List<int>(escapeCardsIndex);
    }

    public static List<ChapterCard> GetChapterCards(int chapter)
    {
        List<ChapterCard> chapterCards = new List<ChapterCard>();
        int[] chapterCardsIndex = chaptersDict[chapter];
        foreach (int index in chapterCardsIndex)
        {
            chapterCards.Add((ChapterCard) cardList[index]);
        }
        return chapterCards;
    }

    public static List<int> GetChapterIndex(int chapter)
    {
        return new List<int>(chaptersDict[chapter]);
    }

    public static List<Card> GetPursuerCards()
    {
        List<Card> pursuerCards = new List<Card>();
        foreach (int index in pursuerCardsIndex)
        {
            pursuerCards.Add(cardList[index]);
        }
        return pursuerCards;
    }

    public static List<int> GetPursuerIndex()
    {
        return new List<int>(pursuerCardsIndex);
    }
}
