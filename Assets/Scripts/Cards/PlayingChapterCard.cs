﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingChapterCard : MonoBehaviour
{
    public bool open = true;
    public GameObject missionPanel;
    //public List<EscapeCard> missionCards = new List<EscapeCard>();
    public EscapeCard lastCard;
    public List<string> remainingColors = new List<string>();
    public ChapterCardDisplay chapterCard;

    /*private void Awake()
    {
        chapterCard = transform.FindChild
    }*/

    // Start is called before the first frame update
    void Start()
    {
        missionPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init()
    {
        open = true;
        missionPanel.SetActive(true);
        foreach (string color in ((ChapterCard) chapterCard.card).colorSequence)
        {
            remainingColors.Add(color);
        }
    }

    public bool CanAddCard(EscapeCard card)
    {
        if (!open) return false;
        if (lastCard != null && card.value < lastCard.value) return false;

        bool colorMatch = false;
        for (int i = 0; !colorMatch && i < remainingColors.Count; ++i)
        {
            Debug.Log("checking color " + remainingColors[i] + " against " + card.color);
            if (remainingColors[i] == card.color)
            {
                colorMatch = true;
            }
        }
        return colorMatch;
    }

    public void AddCard(GameObject card)
    {
        EscapeCardDisplay cd = card.GetComponent<EscapeCardDisplay>();
        cd.onHand = false;
        lastCard = (EscapeCard) cd.card;

        //Instantiate
        card.transform.SetParent(missionPanel.transform);

        // Duplicated code
        for (int i = 0; i < remainingColors.Count; ++i)
        {
            if (remainingColors[i] == lastCard.color)
            {
                remainingColors.RemoveAt(i);
                break;
            }
        }
        CheckMissionComplete();
    }

    void CheckMissionComplete()
    {
        open = remainingColors.Count > 0;

        // Mission complete
        if (!open)
        {
            GameControl.MoveCharacter(((ChapterCard) chapterCard.card).tilesToMove);
            StartCoroutine(CompleteMission());
        }
    }

    IEnumerator CompleteMission()
    {
        GetComponent<CanvasGroup>().alpha = 0.5f;
        // Escape cards to Discard Deck
        foreach (Transform child in missionPanel.transform)
        {
            Debug.Log(child);

            EscapeCardDisplay card = child.GetComponent<EscapeCardDisplay>();
            if (card != null)
            {
                card.card.Print();
                Deck.Discard(card.cardId);
            }
        }

        yield return new WaitForSeconds(1.0f);
        // Destroy this
        Destroy(gameObject);
    }
}
