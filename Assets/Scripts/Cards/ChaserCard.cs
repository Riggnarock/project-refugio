﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
enum CardType
{
    numerical = 0,
    chapters
}
*/

[CreateAssetMenu(fileName = "New Chaser Card", menuName = "Card/ChaserCard")]
public class ChaserCard : Card
{
    public int tilesToMove;
    public string type;

    public ChaserCard Init(int id, int value, string type)
    {
        this.id = id;
        this.cardType = Card.CardType.PURSUER;
        tilesToMove = value;
        // this.type = type == "normal" ? CardType.normal : CardType.chapters;
        this.type = type;

        return this;
    }

    public override void Print()
    {
        Debug.Log("Pursuer card " + id + " type " + type + " value " + tilesToMove);
    }
}
