﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChapterCardDisplay : CardChapterDisplay
{
    public bool isOpen;
    public Image chapterImage;
    public Text chapterName;
    public Text stepsText;
    public ChapterMission mission;

    // Start is called before the first frame update
    protected override void Start()
    {
        card = (ChapterCard)CardDataBase.cardList[cardId];
        chapterImage.sprite = ((ChapterCard)card).artwork;
        chapterName.text = ((ChapterCard)card).chapterName;
        stepsText.text = ((ChapterCard)card).tilesToMove.ToString();
        //Debug.Log("chapter init colors");
        //foreach (string color in ((ChapterCard)card).colorSequence) Debug.Log(color);
        mission.Init(((ChapterCard)card).colorSequence);
        base.Start();
    }
}
