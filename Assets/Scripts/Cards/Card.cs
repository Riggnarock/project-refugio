﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ScriptableObject
{
    public int id;
    public enum CardType
    {
        NONE,
        ESCAPE,
        CHAPTER,
        PURSUER,
        ALL
    };

    public CardType cardType = CardType.NONE;

    public virtual void Print()
    {
        Debug.Log("Card " + id);
    }

    public virtual Card Init()
    {
        return this;
    }
}
