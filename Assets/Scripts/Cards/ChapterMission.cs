﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChapterMission : MonoBehaviour
{
    public Image[] requirements;

    public void Init(string [] sequence)
    {
        int i = 0;
        foreach (string color in sequence)
        {
            requirements[i].enabled = true;
            requirements[i].sprite = Resources.Load<Sprite>("Images/Cards/m_" + color.ToLower());
            i++;
        }
    }
}
