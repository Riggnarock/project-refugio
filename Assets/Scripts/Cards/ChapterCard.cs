﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Chapter Card", menuName = "Card/ChapterCard")]
public class ChapterCard : Card
{
    public int tilesToMove;
    public string chapterName;
    public string[] colorSequence;
    public Sprite artwork;

    public ChapterCard Init(int id, string chapter, int value, string[] colors)
    {
        this.id = id;
        this.cardType = Card.CardType.CHAPTER;
        chapterName = chapter;
        tilesToMove = value;
        colorSequence = colors;
        return this;
    }

    public override void Print()
    {
        string seq = "";
        foreach (string c in colorSequence) seq += c + ", ";
        Debug.Log("chapter card " + id + " " + chapterName + " steps " + tilesToMove + " sequence " + seq);
    }
}
