﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscapeCardDisplay : CardDisplay
{
    //public new EscapeCard card;
    public Image frontImage;
    public Text[] valueTexts;
    public Image chapterImage;
    public Text chapterText;

    // Start is called before the first frame update
    protected override void Start()
    {
        card = (EscapeCard) CardDataBase.cardList[cardId];
        foreach (Text valueText in valueTexts)
            valueText.text = ((EscapeCard)card).value.ToString();
        chapterImage.sprite = ((EscapeCard)card).artwork;
        frontImage.sprite = Resources.Load<Sprite>("Images/Cards/" + ((EscapeCard)card).color);
        chapterText.text = ((EscapeCard)card).chapter;

        base.Start();
    }
}
