﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardDisplay : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int cardId;
    public Card card;
    public GameObject front;
    public GameObject back;
    public bool tapped = true;
    public bool onHand = false;
    public bool dragging = false;
    public Vector3 initialScale;
    public Vector3 initialPosition;
    public float scaleAmount;
    private Vector3 selectedScale;

    private void Awake()
    {
        //front = transform.Find("Front").GetComponent<Image>();
        //back = transform.Find("Back").GetComponent<Image>();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        initialPosition = transform.position;
        initialScale = transform.localScale;
        selectedScale = new Vector3(scaleAmount, scaleAmount, 0);
        UpdateFrontBack();
    }

    // Update is called once per frame
    void Update()
    {
        // DEBUG
        UpdateFrontBack();
    }

    public void Flip()
    {
        tapped = !tapped;
        UpdateFrontBack();
    }

    private void UpdateFrontBack()
    {
        front.SetActive(!tapped);
        back.SetActive(tapped);
    }
    /*
    private void ShowFront()
    {
        front.SetActive(true);
        back.SetActive(false);
    }

    private void ShowBack()
    {
        back.SetActive(true);
        front.SetActive(false);
    }
    */
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (onHand && !dragging)
            transform.localScale += selectedScale;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (onHand && !dragging)
            transform.localScale = initialScale;
    }
    
    public void OnBeginDrag (PointerEventData eventData)
    {
        if (onHand)
        {
            dragging = true;
            gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
            initialPosition = transform.position;
            transform.localScale = initialScale;
        }
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (onHand)
        {
            transform.position = eventData.position;
        }
    }
    
    public void OnEndDrag(PointerEventData eventData)
    {
        if (dragging)
        {
            dragging = false;
        }
        if (onHand)
        {
            gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
            transform.position = initialPosition;
        }
    }
    
}
