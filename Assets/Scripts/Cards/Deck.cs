﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public Card card;
    public int currentChapter = 0;
    public static List<int> currentChapterDeck;
    //public List<EscapeCard> escapeDeck;
    //public List<ChaserCard> pursuerDeck;
    public static List<int> playingDeck;
    public static List<int> discardDeck;

    public GameObject[] cardsInDeck;
    public int deckSize;
    public int discardDeckSize;
    public int initialDeckSize;
    public int cardsToServe = 7;
    public int maxHandSize = 8;
    public static int maxHand;
    public float handScaleAmount = 1.0f;
    static Vector3 handScale;

    public GameObject playerHand;
    public GameObject chapterPanel;
    public GameObject escapePrefab;
    public GameObject pursuerPrefab;
    public GameObject chapterPrefab;

    static GameObject hand;
    static GameObject escape;
    static GameObject pursuer;
    static GameObject chapter;
    static GameObject chapters;


    // Awake is called when the object is instantiated
    private void Awake()
    {
        maxHand = maxHandSize;
        hand = playerHand;
        chapters = chapterPanel;
        escape = escapePrefab;
        pursuer = pursuerPrefab;
        chapter = chapterPrefab;

        handScale = new Vector3(handScaleAmount, handScaleAmount, 1.0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        // Empty discard deck
        discardDeck = new List<int>();

        // Get Escape Cards
        playingDeck = CardDataBase.GetEscapeIndex();

        // Get current Chapter Cards
        currentChapterDeck = CardDataBase.GetChapterIndex(currentChapter);

        // Shuffle decks
        Shuffle(playingDeck);
        Shuffle(currentChapterDeck);

        SetChapterDeck();

        // Initial Serving Cards
        StartCoroutine(InitialDeal());

        // Get Pursuer Cards
        List<int> pursuerIndex = CardDataBase.GetPursuerIndex();
        Debug.Log(pursuerIndex.Count.ToString() + " pursuer cards");

        // Shuffle and pick X (depending on difficulty level)
        Shuffle(pursuerIndex);

        // Add them at random indexes in ranges of Escape Cards

        initialDeckSize = playingDeck.Count;
        deckSize = initialDeckSize;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (playingDeck.Count < initialDeckSize / 2)
        {
            
        }*/
        deckSize = playingDeck.Count;
        discardDeckSize = discardDeck.Count;

        // DEBUG ONLY
        if (deckSize < 0) deckSize = 0;

        int threshold = cardsInDeck.Length;
        if (deckSize < 6)
        {
            threshold = deckSize;
        }
        else if (deckSize < 10)
        {
            threshold = (int)(cardsInDeck.Length * 0.6);
        }
        else if (deckSize < 20)
        {
            threshold = (int) (cardsInDeck.Length * 0.8);
        }
        for (int i = cardsInDeck.Length - 1; i >= threshold; --i)
        {
            cardsInDeck[i].SetActive(false);
        }
    }

    public void LoadChapterDeck(int chapter)
    {

    }

    /*List<T> LoadCards<T>(int first, int last, ref List<T> deck)
    {
        for (int i = first; i <= last; ++i)
        {
            deck[i] = (T) CardDataBase.cardList[i];
        }
    }*/

    void Shuffle<T> (List<T> list)
    {
        System.Random random = new System.Random();
        int n = list.Count;
        while (n > 1)
        {
            int k = random.Next(n);
            n--;
            T temp = list[k];
            list[k] = list[n];
            list[n] = temp;
        }
    }

    void ShuffleArray<T>(T[] list)
    {
        System.Random random = new System.Random();
        int n = list.Length;
        while (n > 1)
        {
            int k = random.Next(n);
            n--;
            T temp = list[k];
            list[k] = list[n];
            list[n] = temp;
        }
    }

    IEnumerator InitialDeal()
    {
        // Serve Players X cards (initial)
        for (int i = 0; i < cardsToServe; ++i)
        {
            yield return new WaitForSeconds(0.2f);

            ServeEscapeCard(playingDeck[i]);
        }
        playingDeck.RemoveRange(0, cardsToServe);

        // Serve Players 1 Chapter Card
        int n = 1;
        //int total = currentChapterDeck.Count;
        for (int i = 0; i < n; ++i)
        {
            yield return new WaitForSeconds(0.2f);
            ServeChapterCard(i);
        }
        currentChapterDeck.RemoveRange(0, n);

        // TESTING ONLY
        /*for (int i = 0; i < 1; ++i)
        {
            GameObject newCard = Instantiate(pursuerPrefab, playerHand.transform);
            newCard.GetComponent<ChaserCardDisplay>().onHand = true;
            newCard.GetComponent<ChaserCardDisplay>().cardId = playingDeck[i];
            newCard.transform.localScale = new Vector3(0.55f, 0.55f, 1.0f);
            newCard.GetComponent<ChaserCardDisplay>().Flip();
        }*/
    }

    public static void DrawEscapeCard()
    {
        if (playingDeck.Count > 0 && hand.transform.childCount < maxHand)
        {
            ServeEscapeCard(playingDeck[0]);
            playingDeck.RemoveAt(0);
        }
    }

    static void ServeEscapeCard(int cardId)
    {
        GameObject newCard = Instantiate(escape, hand.transform);
        newCard.GetComponent<EscapeCardDisplay>().onHand = true;
        newCard.GetComponent<EscapeCardDisplay>().cardId = cardId;
        newCard.transform.localScale = handScale;
        newCard.GetComponent<EscapeCardDisplay>().Flip();
        
    }

    void ServePlayerEscapeCards(int n)
    {
        for (int i = 0; i < n; ++i)
        {
            ServeEscapeCard(playingDeck[i]);
        }
        playingDeck.RemoveRange(0, n);
    }

    public static void DrawChapterCard()
    {
        if (currentChapterDeck.Count > 0 && hand.transform.childCount < maxHand)
        {
            ServeChapterCard(0);
            currentChapterDeck.RemoveAt(0);
        }
    }

    static void ServeChapterCard(int i)
    {
        /*
        GameObject newCard = Instantiate(chapterPrefab, playerHand.transform);
        newCard.GetComponent<ChapterCardDisplay>().onHand = true;
        newCard.GetComponent<ChapterCardDisplay>().cardId = cardId;
        newCard.transform.localScale = new Vector3(0.55f, 0.55f, 1.0f);
        newCard.GetComponent<ChapterCardDisplay>().Flip();
        */
        Transform card = chapters.transform.GetChild(i);
        card.SetParent(hand.transform);
        card.localScale = handScale;
        card.GetComponent<ChapterCardDisplay>().onHand = true;
        card.GetComponent<ChapterCardDisplay>().initialScale = card.localScale;
        card.GetComponent<ChapterCardDisplay>().Flip();
    }

    void ServePlayerChapterCards(int n)
    {
        int total = currentChapterDeck.Count - 1;
        for (int i = 0; i < n; ++i)
        {
            ServeChapterCard(i);

            //Destroy(chapterPanel.transform.GetChild(total - i).gameObject);
        }
        currentChapterDeck.RemoveRange(0, n);
    }

    static void SetChapterDeck()
    {
        for (int i = currentChapterDeck.Count-1; i >= 0; --i)
        {
            GameObject c = Instantiate(chapter, chapters.transform);
            c.GetComponent<ChapterCardDisplay>().cardId = currentChapterDeck[i];
            c.transform.localScale = new Vector3(0.42f, 0.42f, 1.0f);
        }
    }

    public static void Discard(int cardId)
    {
        discardDeck.Add(cardId);
    }
}
