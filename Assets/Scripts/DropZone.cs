﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler
{
    public Card.CardType zoneType = Card.CardType.NONE;
    public float scaleAmount = 1.0f;
    protected Vector3 scaleVec;

    // Start is called before the first frame update
    void Start()
    {
        scaleVec = new Vector3(scaleAmount, scaleAmount, 1);
    }

    public void OnDrop(PointerEventData eventData)
    {
        print(eventData.pointerDrag.name + "was dropped on " + gameObject.name);

        CardDisplay card = eventData.pointerDrag.GetComponent<CardDisplay>();
        if (card != null && (zoneType == Card.CardType.ALL || card.card.cardType == zoneType)) {
            card.card.Print();
            print(card.card.cardType);
            OnDropAction(eventData.pointerDrag);
        }
    }

    public virtual void OnDropAction(GameObject obj)
    {
        return;
    }
}
