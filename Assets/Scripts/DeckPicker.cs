﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DeckPicker : MonoBehaviour, IPointerClickHandler
{
    public Card.CardType deckType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (deckType == Card.CardType.ESCAPE) Deck.DrawEscapeCard();
        else if (deckType == Card.CardType.CHAPTER) Deck.DrawChapterCard();
    }
}
