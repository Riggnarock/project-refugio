﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardDropZone : DropZone
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnDropAction(GameObject obj)
    {
        CardDisplay card = obj.GetComponent<CardDisplay>();

        // discard this card from the player hand
        print("Discard");
        card.card.Print();
        Deck.Discard(card.cardId);

        // Destroy the card object
        Destroy(obj);
    }
}
