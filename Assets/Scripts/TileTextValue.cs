﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TileTextValue : MonoBehaviour
{
    private TextMeshPro textValue;

    private void Awake()
    {
        textValue = GetComponent<TextMeshPro>();
    }
    
    public void Setup(string value)
    {
        if (int.Parse(value) > 0) textValue.SetText(value);
    }
}
