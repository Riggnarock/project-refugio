﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateTiles : MonoBehaviour
{
    public int initialValue = 0;
    GameControl gameControl;

    private void Awake()
    {
        //gameControl = GameObject.Find("GameController").GetComponent<GameControl>();
    }

    private void Start()
    {
        bool pursuerInitiated = false;
        for (int i = 0; i < transform.childCount; ++i)
        {
            Tile currentTile = transform.GetChild(i).GetComponent<Tile>();
            Tile nextTile = null;
            try
            {
                nextTile = transform.GetChild(i + 1).GetComponent<Tile>();
            } catch
            {
                Debug.Log("final tile reached");
            }
            currentTile.Setup(initialValue + i, nextTile);

            if (currentTile.characterInit)
            {
                print("Setting initial tile for character");
                // set Character intial tile
                GameControl.SetupCharacterTile(currentTile);
            }
            if (currentTile.pursuerInit)
            {
                print("Setting initial tile for pursuer");
                // set Pursuer intial tile
                GameControl.SetupPursuerTile(currentTile);
                pursuerInitiated = true;
            }
        }
        
        if (!pursuerInitiated)
        {
            GameControl.SetupPursuerTile(null);
        }
    }
}
