﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    public static bool gameOver = false;
    static TileMovement character;
    static TileMovement pursuer;
    static GameObject gameOverText;
    static GameObject tileManager;

    public int playerTurn = 0;

    private void Awake()
    {
        gameOverText = GameObject.Find("GameOverText");
        gameOverText.SetActive(false);
        character = GameObject.Find("Character").GetComponent<TileMovement>();
        pursuer = GameObject.Find("Pursuer").GetComponent<TileMovement>();
        tileManager = GameObject.Find("TileManager");
    }


    // Start is called before the first frame update
    /*void Start()
    {
        print("GameControl started");

    }*/

    // Update is called once per frame
    void Update()
    {
        if (!character.canMove && pursuer.currentTile == character.currentTile)
        {
            gameOver = true;
            gameOverText.SetActive(true);
            character.canMove = false;
            pursuer.canMove = false;
        }
        /*if (!character.GetComponent<TileMovement>().canMove && pursuer.GetComponent<TileMovement>().waypointIndex >= character.GetComponent<TileMovement>().waypointIndex)
        {
            gameOver = true;
            gameOverText.SetActive(true);
            character.GetComponent<TileMovement>().canMove = false;
            pursuer.GetComponent<TileMovement>().canMove = false;
        }*/

        if (Input.GetKeyUp(KeyCode.Keypad1))
        {
            MoveCharacter(1);
        }
        if (Input.GetKeyUp(KeyCode.Keypad2))
        {
            MoveCharacter(2);
        }
    }

    public static void SetupCharacterTile(Tile tile)
    {
        character.Setup(tile);
    }

    public static void SetupPursuerTile(Tile tile)
    {
        if (tile == null)
        {
            // get last pursuer tile value
            // tile is the max between the last tile value and the first tile value of the board
            // TODO: change this
            pursuer.Setup(tileManager.transform.GetChild(0).GetComponent<Tile>());
        }
        pursuer.Setup(tile);
    }

    public void TestMove(int n)
    {
        MoveCharacter(n);
    }

    public void TestChase(int n)
    {
        MovePursuer(n);
    }

    public void TestSetCharacterTile(int n)
    {
        character.Setup(tileManager.transform.GetChild(n).GetComponent<Tile>());
    }
    public void TestSetPursuerTile(int n)
    {
        pursuer.Setup(tileManager.transform.GetChild(n).GetComponent<Tile>());
    }

    public static void MoveCharacter(int numTiles)
    {
        character.canMove = true;
        character.SetMoves(numTiles);
    }
    public static void MovePursuer(int numTiles)
    {
        pursuer.canMove = true;
        pursuer.SetMoves(numTiles);
    }
}
